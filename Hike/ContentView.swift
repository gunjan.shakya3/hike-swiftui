//
//  ContentView.swift
//  Hike
//
//  Created by Dialaxy Gunjan on 16/04/2024.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        CardView()
    }
}

#Preview {
    ContentView()
}
