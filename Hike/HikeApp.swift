//
//  HikeApp.swift
//  Hike
//
//  Created by Dialaxy Gunjan on 16/04/2024.
//

import SwiftUI

@main
struct HikeApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
